import 'package:auto_route/auto_route.dart';
import 'package:bti_test_flutter/config/themes/color_theme.dart';
import 'package:bti_test_flutter/config/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

@RoutePage()
class LandingScreen extends StatelessWidget {
  const LandingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    Future<void> launchContactsApp(Uri url) async {
      if (!await launchUrl(url, mode: LaunchMode.externalApplication)) {
        throw Exception('Could not launch $url');
      }
    }

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      drawer: Drawer(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Menu',
              style: titleTextStyle,
            ),
            const Divider(
              height: 20,
              color: greyColor,
              thickness: 2,
            ),
            ListTile(
              onTap: () {
                Navigator.of(context).pop();
                AutoRouter.of(context).pushNamed('/login');
              },
              leading: const Icon(Icons.person),
              title: Text(
                'Login',
                style: bodyLgTextStyle.copyWith(fontWeight: FontWeight.w700),
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context).pop();
                AutoRouter.of(context).pushNamed('/map');
              },
              leading: const Icon(Icons.map),
              title: Text(
                'Share Location',
                style: bodyLgTextStyle.copyWith(fontWeight: FontWeight.w700),
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context).pop();
                launchContactsApp(Uri.parse('tel:'));
              },
              leading: const Icon(Icons.contact_page_rounded),
              title: Text(
                'Contacts',
                style: bodyLgTextStyle.copyWith(fontWeight: FontWeight.w700),
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.of(context).pop();
                AutoRouter.of(context).pushNamed('/media');
              },
              leading: const Icon(Icons.perm_media_rounded),
              title: Text(
                'Media',
                style: bodyLgTextStyle.copyWith(fontWeight: FontWeight.w700),
              ),
            ),
          ],
        ),
      )),
      body: Container(
          height: screenHeight,
          width: screenWidth,
          color: Theme.of(context).colorScheme.primary,
          child: const Center(
            child: Text(
              "Ah, you're finally awake.\n"
              "Welcome!",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w700, fontSize: 24, letterSpacing: 8),
            ),
          )),
    );
  }
}
