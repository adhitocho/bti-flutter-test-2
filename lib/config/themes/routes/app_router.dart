import 'package:auto_route/auto_route.dart';
import 'package:bti_test_flutter/app/auth/auth_gate.dart';
import 'package:bti_test_flutter/app/landing/landing_screen.dart';
import 'package:bti_test_flutter/app/map/map_screen.dart';
import 'package:bti_test_flutter/app/media/media_screen.dart';

part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {

  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          path: '/login',
          page: AuthGateRoute.page,
        ),
        AutoRoute(
          path: '/',
          page: LandingRoute.page,
        ),
        AutoRoute(
          path: '/map',
          page: MapRoute.page,
        ),
        AutoRoute(
          path: '/media',
          page: MediaRoute.page,
        ),
      ];
}
