import 'package:bti_test_flutter/config/themes/color_theme.dart';
import 'package:flutter/material.dart';

const TextStyle displayLgTextStyle = TextStyle(
  color: blackColor,
  fontSize: 48,
  fontWeight: FontWeight.w700,
);

const TextStyle displayTextStyle = TextStyle(
  color: blackColor,
  fontSize: 44,
  fontWeight: FontWeight.w700,
);

const TextStyle displaySmTextStyle = TextStyle(
  color: blackColor,
  fontSize: 40,
  fontWeight: FontWeight.w600,
);

const TextStyle headlineLgTextStyle = TextStyle(
  color: blackColor,
  fontSize: 36,
  fontWeight: FontWeight.w700,
);

const TextStyle headlineTextStyle = TextStyle(
  color: blackColor,
  fontSize: 24,
  fontWeight: FontWeight.w700,
);

const TextStyle headlineSmTextStyle = TextStyle(
  color: blackColor,
  fontSize: 20,
  fontWeight: FontWeight.w600,
);

const TextStyle titleLgTextStyle = TextStyle(
  color: blackColor,
  fontSize: 28,
  fontWeight: FontWeight.w500,
);

const TextStyle titleTextStyle = TextStyle(
  color: blackColor,
  fontSize: 24,
  fontWeight: FontWeight.w700,
);

const TextStyle titleSmTextStyle = TextStyle(
  color: blackColor,
  fontSize: 20,
  fontWeight: FontWeight.w700,
);

const TextStyle labelLgTextStyle = TextStyle(
  color: blackColor,
  fontSize: 14,
  fontWeight: FontWeight.w400,
);

const TextStyle labelTextStyle = TextStyle(
  color: blackColor,
  fontSize: 10,
  fontWeight: FontWeight.w400,
);

const TextStyle labelSmTextStyle = TextStyle(
  color: blackColor,
  fontSize: 8,
  fontWeight: FontWeight.w400,
);

const TextStyle bodyLgTextStyle = TextStyle(
  color: blackColor,
  fontSize: 16,
  fontWeight: FontWeight.w500,
);

const TextStyle bodyTextStyle = TextStyle(
  color: blackColor,
  fontSize: 14,
  fontWeight: FontWeight.w400,
);

const TextStyle bodySmTextStyle = TextStyle(
  color: blackColor,
  fontSize: 12,
  fontWeight: FontWeight.w400,
);
