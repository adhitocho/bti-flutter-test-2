import 'package:flutter/material.dart';

/// App primary color
const Color primaryColor = Color(0xff96bf7c);
const Color darkerPrimaryColor = Color(0xff4e196a);
const Color secondaryColor = Color(0xffff8b00);
const Color darkerSecondaryColor = Color(0xfffd5417);
const Color tertiaryColor = Color(0xffffffff);

const Color errorColor = Color(0xffE9304B);
const Color successColor = Color(0xff67CC90);
const Color warningColor = Color(0xffF2A73D);
const Color disabledColor = Color(0xff828282);
const Color greyColor = Color(0xff828282);
const Color lightGreyColor = Color(0xffCCCCCC);
const Color blackColor = Color(0xff424242);
