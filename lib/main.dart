import 'package:bti_test_flutter/config/themes/color_theme.dart';
import 'package:bti_test_flutter/config/themes/routes/app_router.dart';
import 'package:bti_test_flutter/config/themes/text_styles.dart';
import 'package:bti_test_flutter/firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    AppRouter appRouter = AppRouter();

    return MaterialApp.router(
      routerConfig: appRouter.config(),
      title: 'BTI Flutter Test',
      theme: ThemeData(
        colorScheme: const ColorScheme.light(
          primary: primaryColor,
          secondary: secondaryColor,
          tertiary: tertiaryColor,
          error: errorColor,
        ),
        useMaterial3: true,
        primaryColor: primaryColor,
        inputDecorationTheme: InputDecorationTheme(
            contentPadding:
                const EdgeInsets.symmetric(horizontal: 18, vertical: 10),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: const BorderSide(color: lightGreyColor)),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            hintStyle: bodyTextStyle.copyWith(color: greyColor),
            helperStyle: bodySmTextStyle.copyWith(color: disabledColor),
            iconColor: blackColor,
            errorStyle: bodySmTextStyle.copyWith(color: errorColor)),
      ),
    );
  }
}
